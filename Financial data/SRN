import csv
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, r2_score, mean_squared_error
import numpy as np
import numpy.random as r
import matplotlib.pyplot as plt
import random
import operator
from datetime import datetime
from matplotlib.backends.backend_pdf import PdfPages

random.seed(25)

def readcsv(filename):
    ifile = open(filename, "rU")
    reader = csv.reader(ifile, delimiter=",")
    next(reader, None)
    next(reader, None)

    rownum = 0
    dataset = []

    for row in reader:
        row = [float(i) for i in row]
        dataset.append(row)
        rownum += 1
    return [dataset, rownum]


def split_data(data, split_no):
    no = int(split_no * len(data))
    train = data[0:no]
    test = data[no:]
    return train, test

def define_data(data, window_size, horizon):
    raw_output = data[:, 0:2]
    input = []
    output = []
    no = (len(data) - (window_size + horizon - 1)) + 1
    for i in range(no):
        seq = data[i: (i + window_size)]
        if len(seq) == window_size:
            input.append(seq)
            seq = raw_output[i + horizon: (i + horizon + window_size)]
            if len(seq) == window_size:
                output.append(seq)
            else:
                input.pop(-1)
                break
        else:
            break
    return input, output

def sigmoid(x):
    return 1 / (1 + np.exp(-x))

def sigmoid_deriv(x):
    return x * (1 - x)

def initialise_weights(nn_structure):
    weights = {}
    bias = {}
    context = {}
    context[0] =  0.5 * np.ones((nn_structure[1]))
    c_weights = {1: np.ones((nn_structure[1], nn_structure[1]))}
    c = []
    for i in range(nn_structure[1]):
        w = [random.uniform(-0.03, 0.03) for i in range(nn_structure[1])]
        c.append(w)
    c_weights[1] = np.array(c)
    for l in range(1, len(nn_structure)):
        q = []
        for j in range(1, nn_structure[l] + 1):
            w = [random.uniform(-0.03, 0.03) for i in range(nn_structure[l-1])]
            q.append(w)
        weights[l] = np.array(q)
        bias[l] = np.random.uniform(-1, 1, nn_structure[l]) #np.ones((nn_structure[l],))#r.random_sample((nn_structure[l],)) #np.zeros((nn_structure[l],))
    #print('set to 1')
    return weights, bias, c_weights, context

def initialise_weights_changes(nn_structure):
    deltaweights = {}
    deltabias = {}
    deltac_weights = {1: np.zeros((nn_structure[1], nn_structure[1]))}
    for l in range(1, len(nn_structure)):
        deltaweights[l] = np.zeros((nn_structure[l], nn_structure[l-1]))
        deltabias[l] = np.zeros((nn_structure[l],))
    return deltaweights, deltabias, deltac_weights

def calculate_out_layer_delta(y, hidden_layer):
    return -(y - hidden_layer)

def calculate_hidden_delta(delta_plus_1, weights_l):
    return np.dot(np.transpose(weights_l), delta_plus_1)

def feed_forward(i, weights, bias, c_weights, context, k, hidden_layer):
    hidden_layer[k] = {1: i}
    activations = {}
    for l in range(1, len(weights) + 1):
        node_in = hidden_layer[k][l]
        if l == 1:
            activations[l+1] = sigmoid(weights[l].dot(node_in) + c_weights[l].dot(context[k]) + bias[l])
        else:
            activations[l+1] = weights[l].dot(node_in) + bias[l]
        hidden_layer[k][l+1] = np.array(activations[l+1])
        if l == 1:
            context[k+1] = hidden_layer[k][l+1]
    return hidden_layer


def train_nn(nn_structure, train, iter_num, window_size, horizon, eta, momentum,  reg):

    weights, bias, c_weights, context = initialise_weights(nn_structure)

    cnt = 1
    avg_cost_func = []
    print('Starting gradient descent for {} iterations'.format(iter_num))
    while cnt < iter_num:
        if cnt%1000 == 0:
           print('Iteration {} of {}'.format(cnt, iter_num))
        deltaweights, deltabias, deltac_weights = initialise_weights_changes(nn_structure)
        avg_cost = 0
        input, output = define_data(train, window_size, horizon)

        index = list(range(len(input)))
        for m in np.array(range(len(index))):
            hidden_layer = {}
            delta = {}
            choice = np.random.choice(index)
            seq = input[choice]
            seq_out = output[choice]
            k = 0
            for i in seq:
                # perform the feed forward pass and return the stored h and z values, to be used in the gradient descent step
                hidden_layer = feed_forward(i, weights, bias, c_weights, context, k, hidden_layer)
                k += 1

            index.remove(choice)

            #backpropagating the errors for each sequence
            for j in range(len(seq) - 1, -1, -1):
                delta[j] = {}
                if j == len(seq) - 1:
                    for l in range(len(nn_structure), 0, -1):
                        if l == len(nn_structure):
                            delta[j][l] = calculate_out_layer_delta(seq_out[j], hidden_layer[j][l])
                            avg_cost += (seq_out[j] - hidden_layer[j][l])** 2
                        else:
                            if l > 1:
                                delta[j][l] = calculate_hidden_delta(delta[j][l + 1], weights[l]) * sigmoid_deriv(hidden_layer[j][l])

                else:
                    for l in range(len(nn_structure), 0, -1):
                        if l == len(nn_structure):
                            delta[j][l] = calculate_out_layer_delta(seq_out[j], hidden_layer[j][l])
                            avg_cost += (seq_out[j] - hidden_layer[j][l])** 2
                        else:
                            if l > 1:
                                delta[j][l] = calculate_hidden_delta(delta[j][l + 1], weights[l]) + calculate_hidden_delta(delta[j + 1][l], c_weights[1]) * sigmoid_deriv(hidden_layer[j][l])

                for l in range(len(nn_structure) - 1, 0, -1):
                    deltaweights[l] = (-eta * np.dot(delta[j][l + 1][:, np.newaxis], np.transpose(np.array(hidden_layer[j][l])[:, np.newaxis]))) + (momentum * deltaweights[l])
                    deltabias[l] = (-eta * delta[j][l + 1]) + (momentum * deltabias[l])
                    if l == 1:
                        deltac_weights[l] += (-eta * np.dot(delta[j][1 + 1][:, np.newaxis], np.transpose(context[j][:, np.newaxis]))) + (momentum * deltac_weights[l])

            # perform the weights update for each sequence
            for l in range(len(nn_structure) - 1, 0, -1):
                weights[l] += (1 / window_size * deltaweights[l])
                bias[l] += (1 / window_size * deltabias[l])
                if l == 1:
                    c_weights[l] += (1 / window_size * deltac_weights[l])

            deltaweights, deltabias, deltac_weights = initialise_weights_changes(nn_structure)
            context[0] = 0.5 * np.ones((nn_structure[1]))

        # complete the average cost calculation
        avg_cost = np.sqrt(((1.0 / len(input)) * avg_cost))
        if cnt % 1000 == 0:
           print('Error', avg_cost)
        avg_cost_func.append(avg_cost)

        cnt += 1

        trained_model = {
            "model": 'SRN with sliding window',
            "layer": nn_structure,
            "eta": eta,
            "momentum": momentum,
            "epoch": iter_num,
            "window_size": window_size,
            "horizon": horizon
        }

    return weights, bias, c_weights, avg_cost_func, trained_model

def predict(data, window_size, horizon, weights, bias, c_weights, train_no):
    input, output = define_data(data, window_size, horizon)
    prediction = np.zeros((len(input), 2))
    actual = np.zeros((len(input), 2))
    k = 0
    for seq in output:
        actual[k] = seq[-1]
        k += 1
    context = {}
    hidden_layer = {}
    seq_cost = 0
    avg_cost = 0
    k = 0
    good_seq = []
    bad_seq = []
    context[0] = 0.5 * np.ones((nn_structure[1]))
    train_point = int((len(data)) * train_no) - (window_size - horizon + 1)
    for seq in input:
        for i in range(len(seq)):
            hidden_layer = feed_forward(seq[i], weights, bias, c_weights, context, i, hidden_layer)
            seq_cost += (output[k][i] - hidden_layer[i][3]) ** 2
        seq_cost = seq_cost/len(seq)
        prediction[k][0] = hidden_layer[i][3][0]
        prediction[k][1] = hidden_layer[i][3][1]
        context[0] = 0.5 * np.ones((nn_structure[1]))
        seq_data = {1: seq, 2: np.sum(seq_cost)}
        if np.sum(seq_cost) < 0.08:
            good_seq.append(seq_data)
        else:
            bad_seq.append(seq_data)
        avg_cost += np.sum(seq_cost)
        if k == train_point - 1:
            train_RMSE = np.sqrt(avg_cost/train_point)
            avg_cost = 0
        seq_cost = 0
        k += 1

    test_RMSE = np.sqrt(avg_cost/((len(data)) - train_point))

    return prediction, actual, train_RMSE, test_RMSE, train_point


if __name__ == "__main__":
    # load data and scale
    filename = "C:/Users/n0762538/Downloads/financialdata.csv" #"C:/Users/n0762538/Documents/MG.csv"

    #parameters & hyparameters
    window_size = 36
    train_no = 0.75
    horizon = 1
    nn_structure = [1, 30, 1]
    iter_num = 1000
    eta = 0.01
    momentum = 0.75
    reg = 9e-05

    # initialisation
    data, rownum = readcsv(filename)
    data = np.array(data)
    train, test = split_data(data, train_no)
    nn_structure = [len(data[0]), 4, 2]  #

    predictions = []
    trials = 6

    datestring = datetime.strftime(datetime.now(), '%Y-%m-%d')

    for i in range(1, trials + 1):
        print('Trial {} of {}'.format(i, trials))

        #train NN
        weights, bias, c_weights, avg_cost_func, trained_model = train_nn(nn_structure, train, iter_num, window_size, horizon, eta, momentum, reg)
        prediction, actual, train_RMSE, test_RMSE, check = predict(data, window_size, horizon, weights, bias, c_weights, train_no)
        predictions.append(test_RMSE)

        c = 2
        r = int(trials / c)

        # plot the avg_cost_func
        fig1 = plt.figure(1, figsize=(10, 40))
        fig1.suptitle(('Average Error'))

        # plot the avg_cost_func
        fig1.add_subplot(r, c, i).plot(avg_cost_func)
        plt.title((('Trail {} of {}'.format(i, trials)), str(np.round(np.average(avg_cost_func), 4))))
        plt.ylabel('Average J')
        plt.xlabel('Iteration number')
        plt.pause(0.01)
        plt.show(0)
        plt.subplots_adjust(wspace=0.5, hspace=1.5)

        # FIGURE 2
        fig2 = plt.figure(2, figsize=(10, 40))
        fig2.suptitle('Predictions RMSE')

        # subplot
        fig2.add_subplot(r, c, i)
        actual_ = plt.plot(actual[:,0], label = 'Actual')
        estimate = plt.plot(prediction[:,0], label = 'Prediction')
        plt.legend(bbox_to_anchor=(1, 0.9), loc=4, borderaxespad=0.,fontsize = 'small')
        plt.title((('Trail {} of {}'.format(i, trials)), str(np.round(test_RMSE, 4))))
        plt.ylabel('Prediction')
        plt.xlabel('Mackey Glass')
        plt.axvline(check, color='k')
        plt.pause(0.01)
        plt.show(0)
        plt.subplots_adjust(wspace=0.5, hspace=1.5)

        f = open(str(trained_model['model']) + str(trained_model['layer']) + datestring + '.txt', 'a+')
        if i == 1:
            f.write(('\n'))
            f.write(('\n'))
            f.write((str(datetime.strftime(datetime.now(), '%Y-%m-%d-%H-%M-%S'))))
            for key, value in trained_model.items():
                if key == 'train_RMSE':
                    continue
                if key == 'test_RMSE':
                    continue
                else:
                    f.write(('\n' + str(key) + ' ' + str(trained_model[key])))
        f.write(('\n'))
        f.write((str(('Trail {} of {}'.format(i, trials)))))
        f.write((' ' + 'Average training RMSE:' + ' ' + str(np.round(np.average(avg_cost_func), 4))))
        f.write((' ' + 'Test RMSE:' + ' ' + str(np.round(test_RMSE, 4))))

    f.write(('\n'))
    f.write('Average test RMSE: ')
    f.write((str(np.round(np.average(predictions), 5))))

    # save figures to one pdf
    date = datetime.strftime(datetime.now(), '%Y-%m-%d-%H-%M')
    with PdfPages(str(trained_model['layer']) + date + '.pdf') as pdf:
        pdf.savefig(fig1)
        pdf.savefig(fig2)

    plt.show()





