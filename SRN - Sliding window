import csv
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, r2_score, mean_squared_error
import numpy as np
import numpy.random as r
import matplotlib.pyplot as plt
import random
random.seed(25)

def readcsv(filename):
    ifile = open(filename, "rU")
    reader = csv.reader(ifile, delimiter=",")

    rownum = 0
    dataset = []

    for row in reader:
        dataset.append(row)
        rownum += 1
    data = []
    for s in dataset:
        Dataset = [float(i) for i in s]
        data.append(Dataset)
    return [data, rownum]


def split_data(data, split_no):
    no = int(split_no * len(data))
    train = data[0:no]
    test = data[no:]
    return train, test

def define_data(data, window_size, horizon):
    input = []
    output = []
    no = (len(data) - (window_size + horizon - 1)) + 1
    for i in range(no):
        seq = data[i: (i + window_size)]
        if len(seq) == window_size:
            input.append(seq)
            seq = data[i + horizon: (i + horizon + window_size)]
            if len(seq) == window_size:
                output.append(seq)
            else:
                input.pop(-1)
                break
        else:
            break
    return input, output

def sigmoid(x):
    return 1 / (1 + np.exp(-x))

def sigmoid_deriv(x):
    return x * (1 - x)

def initialise_weights(nn_structure):
    weights = {}
    bias = {}
    context = {}
    context[0] = r.random_sample((nn_structure[1]))
    c_weights = {1: np.ones((nn_structure[1], nn_structure[1]))}
    c = []
    for i in range(nn_structure[1]):
        w = [random.uniform(-0.09, 0.09) for i in range(nn_structure[1])]
        c.append(w)
    c_weights[1] = np.array(c)
    for l in range(1, len(nn_structure)):
        q = []
        for j in range(1, nn_structure[l] + 1):
            w = [random.uniform(-0.03, 0.03) for i in range(nn_structure[l-1])]
            q.append(w)
        weights[l] = np.array(q)
        bias[l] = r.random_sample((nn_structure[l],))
    return weights, bias, c_weights, context

def initialise_weights_changes(nn_structure):
    deltaweights = {}
    deltabias = {}
    deltac_weights = {1: np.zeros((nn_structure[1], nn_structure[1]))}
    for l in range(1, len(nn_structure)):
        deltaweights[l] = np.zeros((nn_structure[l], nn_structure[l-1]))
        deltabias[l] = np.zeros((nn_structure[l],))
    return deltaweights, deltabias, deltac_weights

def calculate_out_layer_delta(y, hidden_layer):
    return -(y - hidden_layer)

def calculate_hidden_delta(delta_plus_1, weights_l):
    return np.dot(np.transpose(weights_l), delta_plus_1)

def feed_forward(i, weights, bias, c_weights, context, k, hidden_layer):
    hidden_layer[k] = {1: i}
    activations = {}
    for l in range(1, len(weights) + 1):
        node_in = hidden_layer[k][l]
        if l == 1:
            activations[l+1] = sigmoid(weights[l].dot(node_in) + c_weights[l].dot(context[k]) + bias[l])
        else:
            activations[l+1] = weights[l].dot(node_in) + bias[l]
        hidden_layer[k][l+1] = np.array(activations[l+1])
        if l == 1:
            context[k+1] = hidden_layer[k][l+1]
    return hidden_layer, activations


def train_nn(nn_structure, train, iter_num, window_size, horizon, eta, momentum,  reg):
    print('Learning rate:', eta)
    print('Momentum:', momentum)
    print('Reg term:', reg)
    print('Window size:', window_size)
    print('horizon:', horizon)

    weights, bias, c_weights, context = initialise_weights(nn_structure)
    cnt = 1
    avg_cost_func = []
    print('Starting gradient descent for {} iterations'.format(iter_num))
    while cnt < iter_num:
        if cnt%1000 == 0:
           print('Iteration {} of {}'.format(cnt, iter_num))
        deltaweights, deltabias, deltac_weights = initialise_weights_changes(nn_structure)
        avg_cost = 0
        input, output = define_data(train, window_size, horizon)
        error = 0
        index = list(range(len(input)))
        for m in np.array(range(len(index))):
            hidden_layer = {}
            delta = {}
            choice = np.random.choice(index)
            seq = input[choice]
            seq_out = output[choice]
            k = 0
            for i in seq:
                # perform the feed forward pass and return the stored h and z values, to be used in the gradient descent step
                hidden_layer, activations = feed_forward(i, weights, bias, c_weights, context, k, hidden_layer)
                k += 1

            index.remove(choice)

            #backpropagating the errors for each sequence
            for j in range(len(seq) - 1, -1, -1):
                delta[j] = {}
                if j == len(seq) - 1:
                    for l in range(len(nn_structure), 0, -1):
                        if l == len(nn_structure):
                            delta[j][l] = calculate_out_layer_delta(seq_out[j], hidden_layer[j][l])
                            avg_cost += np.sqrt(mean_squared_error(seq_out[j], hidden_layer[j][l]))
                        else:
                            if l > 1:
                                delta[j][l] = calculate_hidden_delta(delta[j][l + 1], weights[l]) * sigmoid_deriv(hidden_layer[j][l])

                else:
                    for l in range(len(nn_structure), 0, -1):
                        if l == len(nn_structure):
                            delta[j][l] = calculate_out_layer_delta(seq_out[j], hidden_layer[j][l])
                            avg_cost += np.sqrt(mean_squared_error(seq_out[j], hidden_layer[j][l]))
                        else:
                            if l > 1:
                                delta[j][l] = calculate_hidden_delta(delta[j][l + 1], weights[l]) + calculate_hidden_delta(delta[j + 1][l], c_weights[1]) * sigmoid_deriv(hidden_layer[j][l])

                for l in range(len(nn_structure) - 1, 0, -1):
                    deltaweights[l] = (-eta * np.dot(delta[j][l + 1][:, np.newaxis], np.transpose(np.array(hidden_layer[j][l])[:, np.newaxis]))) + (momentum * deltaweights[l])
                    deltabias[l] = (-eta * delta[j][l + 1]) + (momentum * deltabias[l])
                    if l == 1:
                        deltac_weights[l] += (-eta * np.dot(delta[j][1 + 1][:, np.newaxis], np.transpose(context[j][:, np.newaxis]))) + (momentum * deltac_weights[l])

            # perform the weights update for each sequence
            for l in range(len(nn_structure) - 1, 0, -1):
                weights[l] += (1 / window_size * deltaweights[l])
                bias[l] += (1 / window_size * deltabias[l])
                if l == 1:
                    c_weights[l] += (1 / window_size * deltac_weights[l])

            deltaweights, deltabias, deltac_weights = initialise_weights_changes(nn_structure)
            context[0] = r.random_sample((nn_structure[1]))

            # complete the average cost calculation
            avg_cost = (1.0 / len(input) * avg_cost)
            #if cnt % 1000 == 0:
             #   print('Error', avg_cost)
            avg_cost_func.append(avg_cost)


        cnt += 1

    return weights, bias, c_weights, avg_cost_func

def predict(data, window_size, horizon, weights, bias, c_weights):
    input, output = define_data(data, window_size, horizon)
    prediction = np.zeros((len(input),))
    actual = np.zeros((len(input),))
    k = 0
    for seq in output:
        actual[k] = seq[-1][-1]
        k += 1
    context = {}
    hidden_layer = {}
    seq_cost = 0
    avg_cost = 0
    k = 0
    good_seq = []
    bad_seq = []
    context[0] = r.random_sample((nn_structure[1]))
    for seq in input:
        for i in range(len(seq)):
            hidden_layer, activations = feed_forward(seq[i], weights, bias, c_weights, context, k, hidden_layer)
            avg_cost += (output[k][i] - hidden_layer[k][3]) ** 2 #np.sqrt(mean_squared_error(actual[k], prediction[k]))
            seq_cost += (output[k][i] - hidden_layer[k][3]) ** 2  #(actual[k] - prediction[k]) ** 2 #np.sqrt(mean_squared_error(actual[k], prediction[k]))
        prediction[k] = hidden_layer[k][3]
        seq_data = {1: seq, 2: seq_cost}
        if seq_cost < 0.08:
            good_seq.append(seq_data)
        else:
            bad_seq.append(seq_data)
        context[k] = r.random_sample((nn_structure[1]))
        seq_cost = 0
        k += 1
    print (len(good_seq))
    Error = np.sqrt(avg_cost/len(data))

    return prediction, actual, Error

#parameters & hyparameters
filename = "C:/Users/n0762538/Documents/MG.csv"
window_size = 5
horizon = 1
nn_structure = [1, 10, 1]
iter_num = 1000
eta = 0.001
momentum = 0.7
reg = 9e-05

#initialisation
data, rownum = readcsv(filename)
train, test = split_data(data, 0.65)
input, output = define_data(train, window_size, horizon)
weights, bias, c_weights, avg_cost_func = train_nn(nn_structure, train, iter_num, window_size, horizon, eta, momentum, reg)
prediction, actual, Error = predict(data, window_size, horizon, weights, bias, c_weights)
print('Prediction:', prediction[1:10])
print('Actual:', actual[1:10])

# plot the avg_cost_func
plt.plot(avg_cost_func)
plt.ylabel('Average J')
plt.xlabel('Iteration number')
plt.show()

#plot results
actual = plt.plot(actual, label = 'actual')
predicted = plt.plot(prediction, label = 'predicted')
plt.title(('SRN', Error, nn_structure, eta, iter_num, ('Window size:', window_size), ('horizon:', horizon)))
plt.ylabel('Predicted')
plt.xlabel('Iteration number')
legend = plt.legend(loc='upper right', shadow=False, fontsize='small')
plt.show()
