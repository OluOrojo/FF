import csv

import scipy

from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
import numpy as np
import numpy.random as r
import matplotlib.pyplot as plt
import random

random.seed(25)

from _csv import reader

import pandas as pd

def readcsv(filename):
    ifile = open(filename, "rU")
    reader = csv.reader(ifile, delimiter=",")

    rownum = 0
    dataset = []

    for row in reader:
        dataset.append(row)
        rownum += 1
    data = []
    for s in dataset:
        Dataset = [float(i) for i in s]
        data.append(Dataset)
    data = np.array(data)
    return [data, rownum]

#SPLIT DATASET
def define(raw_data):
    output = []
    data = []
    for s in raw_data:
        O = s[-1]
        output.append(O)
        d = s[0:-1]
        data.append(d)
    return [data, output]

def convert_y_to_vect(y):
    y = np.array(y)
    y = [int(i) for i in y]
    no = len(set(y))
    y_vect = np.zeros((len(y), no))
    for i in range(len(y)):
        y_vect[i, y[i]] = 1
    return y_vect

def sigmoid(x):
    return scipy.special.expit(x)


def sigmoid_deriv(x):
    return x * (1 - x)


def initialise_weights(nn_structure):
    weights = {}
    bias = {}
    context = {0: r.random_sample((nn_structure[1]))}
    for l in range(1, len(nn_structure)):
        weights[l] = r.random_sample((nn_structure[l], nn_structure[l-1]))
        bias[l] = r.random_sample((nn_structure[l],))
        if l == 1:
            c_weights = {l: r.random_sample((nn_structure[l], nn_structure[l]))}
    return weights, bias, c_weights, context


def initialise_weights_changes(nn_structure):
    deltaweights = {}
    deltabias = {}
    for l in range(1, len(nn_structure)):
        deltaweights[l] = np.zeros((nn_structure[l], nn_structure[l-1]))
        deltabias[l] = np.zeros((nn_structure[l],))
        if l == 1:
            deltac_weights = {l: np.zeros((nn_structure[l], nn_structure[l]))}
    return deltaweights, deltabias, deltac_weights

def feed_forward(x, weights, bias, c_weights, context, i, hidden_layer):
    hidden_layer[i] = {1 : x}
    activations = {}
    for l in range(1, len(weights) + 1):
        node_in = hidden_layer[i][l]
        if l == 1:
            activations[l+1] = weights[l].dot(node_in) * c_weights[l].dot(context[l - 1]) + bias[l]
        else:
            activations[l+1] = weights[l].dot(node_in) + bias[l]
        hidden_layer[i][l + 1] = sigmoid(activations[l+1])
        if l == 1:
            context[i] = hidden_layer[i][l+1]
    return hidden_layer, context

def train_nn(nn_structure, X, y, iter_num=3000, alpha=0.25, bptt = 4):
    weights, bias, c_weights, context = initialise_weights(nn_structure)
    deltaweights, deltabias, deltac_weights = initialise_weights_changes(nn_structure)
    cnt = 0
    m = len(y)
    avg_cost_func = []
    print('Starting gradient descent for {} iterations'.format(iter_num))
    while cnt < iter_num:
        if cnt%1000 == 0:
            print('Iteration {} of {}'.format(cnt, iter_num))
        avg_cost = 0
        hidden_layer = {}
        for i in range(len(y)):
            hidden_layer, context = feed_forward(X[i], weights, bias, c_weights, context, i, hidden_layer)
            delta = {}


        for i in range(len(y)-1, 0, -1):
            delta[i] = {}
            for l in range(len(nn_structure), 0, -1):
                    if l == len(nn_structure):
                        delta[i][l] = -(y[i] -  hidden_layer[i][l]) * (sigmoid_deriv(hidden_layer[i][l]))
                        avg_cost += np.linalg.norm((y[i] - hidden_layer[i][l]))

                    else:
                        if l > 1:
                            if i == len(y) - 1:
                                delta[i][l] = np.dot(np.transpose(weights[l]), delta[i][l+1]) * (sigmoid_deriv(hidden_layer[i][l]))
                            else:
                                delta[i][l] = np.dot(np.transpose(weights[l]), delta[i][l+1]) + np.dot(np.transpose(weights[l]), delta[i+1][l+1]) * (sigmoid_deriv(hidden_layer[i][l]))

                        #weight change
                        deltaweights[l] += np.dot(delta[i][l + 1][:, np.newaxis], np.transpose(hidden_layer[i][l][:, np.newaxis]))
                        deltabias[l] += delta[i][l + 1]

                        #hidden context weight change
                        if l == 1:
                            if i < len(y) - 1:
                                deltac_weights[l] += np.dot(delta[i][l + 1][:, np.newaxis], np.transpose(hidden_layer[i - 1][l + 1][:, np.newaxis]))
                                #np.dot(delta[i][l + 1][:, np.newaxis], np.transpose(hidden_layer[i-1][l][:, np.newaxis]))

        for l in range(len(nn_structure) - 1, 0, -1):
            weights[l] += -alpha * (1.0 / bptt * deltaweights[l])
            bias[l] += -alpha * (1.0 / bptt * deltabias[l])
            if l == 1:
                c_weights[l] += -alpha * (1.0 / bptt * deltac_weights[l])

        # complete the average cost calculation
        avg_cost = 1.0 / m * avg_cost
        if cnt % 1000 == 0:
            print('Error', avg_cost)
        avg_cost_func.append(avg_cost)
        cnt += 1
    return weights, bias, avg_cost_func, c_weights, context

def predict_y(weights, bias, X, n_layers, c_weights, context):
    m = X.shape[0]
    y = np.zeros((m,))
    for i in range(m):
        hidden_layer, context = feed_forward(X[i, :],weights, bias, c_weights, context, i, hidden_layer={})
        y[i] = np.argmax(hidden_layer[i][n_layers])
    print(y)
    return y

if __name__ == "__main__":
    # load data and scale
    filename = '/Users/Tams/Downloads/pima-indians-diabetes.csv'
    dataset, rownum = readcsv(filename)
    #np.random.shuffle(dataset)
    data, output = define(dataset)
    # print(data, output)
    X_scale = StandardScaler()
    X = X_scale.fit_transform(data)
    print(len(X[0]))
    y = output

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.4)



    y_v_train = convert_y_to_vect(y_train)
    y_v_test = convert_y_to_vect(y_test)
    # setup the NN structure
    nn_structure = [len(X[0]), 30, len(y_v_test[0])]
    # train the NN
    weights, bias, avg_cost_func, c_weights, context = train_nn(nn_structure, X_train, y_v_train)
    # plot the avg_cost_func
    plt.plot(avg_cost_func)
    plt.ylabel('Average J')
    plt.xlabel('Iteration number')
    plt.show()
    # get the prediction accuracy and print
    #y_test = [i - 1 for i in y_test]
    print('test:', y_v_test)
    y_pred = predict_y(weights, bias, X_test, 3, c_weights, context)
    print('Prediction accuracy is {}%'.format(accuracy_score(y_test, y_pred) * 100))