import random
import math
import numpy as np
random.seed(1)

def initialize(input_num,hidden_num,output_num):
    hidden_weights = [[random.random() for i in range(hidden_num)] for i in range(input_num)]
    output_weights = [[random.random() for i in range(output_num)] for i in range(hidden_num)]
    return [hidden_weights, output_weights]

def sigmoid(x):
    return 1 / (1 + np.exp(-x))

def activate(output_weights, hidden_weights, input, b):
    hidden_activation = sigmoid((np.dot(input , hidden_weights)) + b)
    output_activation = sigmoid((np.dot(hidden_activation , output_weights)) + b)
    return [hidden_activation, output_activation]

def error(prediction, output):
    Error = np.sum(np.multiply((np.power((np.subtract(output , prediction)) , 2)), 0.5))
    return Error

def delta(output , prediction, output_weights, hidden_activation):
    DeltaO = np.sum((output - prediction) * prediction * (1.0 - prediction), axis = 0)
    #print('DeltaO',DeltaO)
    SumD = np.dot (output_weights, DeltaO)
    DeltaH = np.sum(SumD * hidden_activation * (1.0 - hidden_activation), axis = 0)
    #print ('DeltaO:', DeltaO, 'DeltaH', DeltaH)
    return [DeltaO, DeltaH]

def updateweights(hidden_weights, output_weights, alpha, eta, DeltaH, DeltaO):
    DeltaWeightIH = np.multiply((np.multiply(alpha , hidden_weights)) , (np.multiply(eta, DeltaH)) )
    DeltaWeightHO = np.multiply((np.multiply(alpha , output_weights)) , (np.multiply(eta, DeltaO)) )

    #DeltaWeightIH[i][j] = eta * Input[p][i] * DeltaH[j] + alpha * DeltaWeightIH[i][j];
    #print('DeltaWeightIH:', DeltaWeightIH, 'DeltaWeightHO:', DeltaWeightHO)
    return [DeltaWeightIH, DeltaWeightHO]

def training(epoch, input_num, hidden_num, output_num, input, b):
    [hidden_weights, output_weights] = initialize(input_num, hidden_num, output_num)
    runtime = 1
    Error = []
    while runtime < epoch:
        [hidden_activation, prediction] = activate(output_weights, hidden_weights, input, b)
        E = error(prediction, output)
        Error.append(E)
        [DeltaO, DeltaH] = delta(output, prediction, output_weights, hidden_activation)
        [hidden_weights, output_weights] = updateweights(hidden_weights, output_weights, alpha, eta, DeltaH, DeltaO)
        runtime += 1
    return [hidden_weights, output_weights, epoch, Error]


input = [[2.7810836,2.550537003]]#,[1.465489372,2.362125076], [3.396561688,4.400293529], [1.38807019,1.850220317]]

output = [[1, 0],
          [1, 1],
          [1, 0],
          [0, 0]]

input_num = len(input[0])
hidden_num = 3
output_num = 2
b = 0
alpha = 0.6
eta = 0.1
epoch = 100

#[hidden_weights, output_weights] = initialize(input_num,hidden_num,output_num)
#print ('hidden_weights:', hidden_weights, 'output_weights:', output_weights)
#[hidden_activation, prediction] = activate(output_weights, hidden_weights, input, b)
#print('hidden_activation', hidden_activation, 'prediction', prediction)
#Error = error(prediction, output)
#print('Error:', Error)
#[DeltaO, DeltaH] = delta(output, prediction, output_weights, hidden_activation)
#print('DeltaO:', DeltaO, 'DeltaH:', DeltaH)
#updateweights(hidden_weights, alpha, eta, DeltaH, DeltaO)
#print('D:', D)

[hidden_weights, output_weights, epoch, Error] = training(epoch, input_num, hidden_num, output_num, input, b)
print ('hidden_weights:', hidden_weights, 'output_weights:', output_weights, 'epoch:', epoch, 'Error:', Error)